﻿
using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using System.Collections.Generic;

namespace JinianNet.KuaiBlog.SQLServerDAL
{
    /// <summary>
    /// 数据访问类:Blogs
    /// </summary>
    public partial class Blog : BaseDAL
    {
        public Blog()
        { }

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(JinianNet.KuaiBlog.Entity.Blog model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into be_Blogs(");
            strSql.Append("BlogId,BlogName,Hostname,IsAnyTextBeforeHostnameAccepted,StorageContainerName,VirtualPath,IsPrimary,IsActive,IsSiteAggregation)");
            strSql.Append(" values (");
            strSql.Append("@BlogId,@BlogName,@Hostname,@IsAnyTextBeforeHostnameAccepted,@StorageContainerName,@VirtualPath,@IsPrimary,@IsActive,@IsSiteAggregation)");
            strSql.Append(";select @@IdENTITY");
            SqlParameter[] parameters = {
					new SqlParameter("@BlogId", SqlDbType.UniqueIdentifier,16),
					new SqlParameter("@BlogName", SqlDbType.NVarChar,255),
					new SqlParameter("@Hostname", SqlDbType.NVarChar,255),
					new SqlParameter("@IsAnyTextBeforeHostnameAccepted", SqlDbType.Bit,1),
					new SqlParameter("@StorageContainerName", SqlDbType.NVarChar,255),
					new SqlParameter("@VirtualPath", SqlDbType.NVarChar,255),
					new SqlParameter("@IsPrimary", SqlDbType.Bit,1),
					new SqlParameter("@IsActive", SqlDbType.Bit,1),
					new SqlParameter("@IsSiteAggregation", SqlDbType.Bit,1)};
            parameters[0].Value = Guid.NewGuid();
            parameters[1].Value = model.BlogName;
            parameters[2].Value = model.Hostname;
            parameters[3].Value = model.IsAnyTextBeforeHostnameAccepted;
            parameters[4].Value = model.StorageContainerName;
            parameters[5].Value = model.VirtualPath;
            parameters[6].Value = model.IsPrimary;
            parameters[7].Value = model.IsActive;
            parameters[8].Value = model.IsSiteAggregation;

            object obj = Helper.ExecuteScalar(CommandType.Text, strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(JinianNet.KuaiBlog.Entity.Blog model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update be_Blogs set ");
            strSql.Append("BlogId=@BlogId,");
            strSql.Append("BlogName=@BlogName,");
            strSql.Append("Hostname=@Hostname,");
            strSql.Append("IsAnyTextBeforeHostnameAccepted=@IsAnyTextBeforeHostnameAccepted,");
            strSql.Append("StorageContainerName=@StorageContainerName,");
            strSql.Append("VirtualPath=@VirtualPath,");
            strSql.Append("IsPrimary=@IsPrimary,");
            strSql.Append("IsActive=@IsActive,");
            strSql.Append("IsSiteAggregation=@IsSiteAggregation");
            strSql.Append(" where BlogRowId=@BlogRowId");
            SqlParameter[] parameters = {
					new SqlParameter("@BlogId", SqlDbType.UniqueIdentifier,16),
					new SqlParameter("@BlogName", SqlDbType.NVarChar,255),
					new SqlParameter("@Hostname", SqlDbType.NVarChar,255),
					new SqlParameter("@IsAnyTextBeforeHostnameAccepted", SqlDbType.Bit,1),
					new SqlParameter("@StorageContainerName", SqlDbType.NVarChar,255),
					new SqlParameter("@VirtualPath", SqlDbType.NVarChar,255),
					new SqlParameter("@IsPrimary", SqlDbType.Bit,1),
					new SqlParameter("@IsActive", SqlDbType.Bit,1),
					new SqlParameter("@IsSiteAggregation", SqlDbType.Bit,1),
					new SqlParameter("@BlogRowId", SqlDbType.Int,4)};
            parameters[0].Value = model.BlogId;
            parameters[1].Value = model.BlogName;
            parameters[2].Value = model.Hostname;
            parameters[3].Value = model.IsAnyTextBeforeHostnameAccepted;
            parameters[4].Value = model.StorageContainerName;
            parameters[5].Value = model.VirtualPath;
            parameters[6].Value = model.IsPrimary;
            parameters[7].Value = model.IsActive;
            parameters[8].Value = model.IsSiteAggregation;
            parameters[9].Value = model.BlogRowId;

            int rows = Helper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int BlogRowId)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from be_Blogs ");
            strSql.Append(" where BlogRowId=@BlogRowId");
            SqlParameter[] parameters = {
					new SqlParameter("@BlogRowId", SqlDbType.Int,4)
			};
            parameters[0].Value = BlogRowId;

            int rows = Helper.ExecuteNonQuery(CommandType.Text, strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string BlogRowIdlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from be_Blogs ");
            strSql.Append(" where BlogRowId in (" + BlogRowIdlist + ")  ");
            int rows = Helper.ExecuteNonQuery(CommandType.Text, strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public JinianNet.KuaiBlog.Entity.Blog GetItem(int BlogRowId)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 BlogRowId,BlogId,BlogName,Hostname,IsAnyTextBeforeHostnameAccepted,StorageContainerName,VirtualPath,IsPrimary,IsActive,IsSiteAggregation from be_Blogs ");
            strSql.Append(" where BlogRowId=@BlogRowId");
            SqlParameter[] parameters = {
					new SqlParameter("@BlogRowId", SqlDbType.Int,4)
			};
            parameters[0].Value = BlogRowId;


            using (System.Data.Common.DbDataReader dr = Helper.ExecuteReader(CommandType.Text, strSql.ToString(), parameters))
            {
                if (dr.Read())
                {
                    return GetItem(dr);
                }
                else
                {
                    return null;
                }
            }

        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        private JinianNet.KuaiBlog.Entity.Blog GetItem(System.Data.Common.DbDataReader dr)
        {
            JinianNet.KuaiBlog.Entity.Blog model = new JinianNet.KuaiBlog.Entity.Blog();

            if (dr["BlogRowId"] != null && dr["BlogRowId"].ToString() != "")
            {
                model.BlogRowId = int.Parse(dr["BlogRowId"].ToString());
            }
            if (dr["BlogId"] != null && dr["BlogId"].ToString() != "")
            {
                model.BlogId = new Guid(dr["BlogId"].ToString());
            }
            if (dr["BlogName"] != null)
            {
                model.BlogName = dr["BlogName"].ToString();
            }
            if (dr["Hostname"] != null)
            {
                model.Hostname = dr["Hostname"].ToString();
            }
            if (dr["IsAnyTextBeforeHostnameAccepted"] != null && dr["IsAnyTextBeforeHostnameAccepted"].ToString() != "")
            {
                if ((dr["IsAnyTextBeforeHostnameAccepted"].ToString() == "1") || (dr["IsAnyTextBeforeHostnameAccepted"].ToString().ToLower() == "true"))
                {
                    model.IsAnyTextBeforeHostnameAccepted = true;
                }
                else
                {
                    model.IsAnyTextBeforeHostnameAccepted = false;
                }
            }
            if (dr["StorageContainerName"] != null)
            {
                model.StorageContainerName = dr["StorageContainerName"].ToString();
            }
            if (dr["VirtualPath"] != null)
            {
                model.VirtualPath = dr["VirtualPath"].ToString();
            }
            if (dr["IsPrimary"] != null && dr["IsPrimary"].ToString() != "")
            {
                if ((dr["IsPrimary"].ToString() == "1") || (dr["IsPrimary"].ToString().ToLower() == "true"))
                {
                    model.IsPrimary = true;
                }
                else
                {
                    model.IsPrimary = false;
                }
            }
            if (dr["IsActive"] != null && dr["IsActive"].ToString() != "")
            {
                if ((dr["IsActive"].ToString() == "1") || (dr["IsActive"].ToString().ToLower() == "true"))
                {
                    model.IsActive = true;
                }
                else
                {
                    model.IsActive = false;
                }
            }
            if (dr["IsSiteAggregation"] != null && dr["IsSiteAggregation"].ToString() != "")
            {
                if ((dr["IsSiteAggregation"].ToString() == "1") || (dr["IsSiteAggregation"].ToString().ToLower() == "true"))
                {
                    model.IsSiteAggregation = true;
                }
                else
                {
                    model.IsSiteAggregation = false;
                }
            }

            return model;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public IList<Entity.Blog> GetList()
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select BlogRowId,BlogId,BlogName,Hostname,IsAnyTextBeforeHostnameAccepted,StorageContainerName,VirtualPath,IsPrimary,IsActive,IsSiteAggregation ");
            strSql.Append(" FROM be_Blogs ");
            List<Entity.Blog> list = new List<Entity.Blog>();
            using (System.Data.Common.DbDataReader dr = Helper.ExecuteReader(CommandType.Text, strSql.ToString()))
            {
                while (dr.Read())
                {
                    list.Add(GetItem(dr));
                }
            }

            return list;
        }

    
    }
}

