﻿
using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;

namespace JinianNet.KuaiBlog.SQLServerDAL
{
    /// <summary>
    /// 数据访问类:BlogRollItems
    /// </summary>
    public partial class BlogRollItem : BaseDAL
    {
        public BlogRollItem()
        { }
        


        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(JinianNet.KuaiBlog.Entity.BlogRollItem model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into be_BlogRollItems(");
            strSql.Append("BlogId,BlogRollId,Title,Description,BlogUrl,FeedUrl,Xfn,SortIndex)");
            strSql.Append(" values (");
            strSql.Append("@BlogId,@BlogRollId,@Title,@Description,@BlogUrl,@FeedUrl,@Xfn,@SortIndex)");
            strSql.Append(";select @@IdENTITY");
            SqlParameter[] parameters = {
					new SqlParameter("@BlogId", SqlDbType.UniqueIdentifier,16),
					new SqlParameter("@BlogRollId", SqlDbType.UniqueIdentifier,16),
					new SqlParameter("@Title", SqlDbType.NVarChar,255),
					new SqlParameter("@Description", SqlDbType.NVarChar,-1),
					new SqlParameter("@BlogUrl", SqlDbType.VarChar,255),
					new SqlParameter("@FeedUrl", SqlDbType.VarChar,255),
					new SqlParameter("@Xfn", SqlDbType.VarChar,255),
					new SqlParameter("@SortIndex", SqlDbType.Int,4)};
            parameters[0].Value = Guid.NewGuid();
            parameters[1].Value = Guid.NewGuid();
            parameters[2].Value = model.Title;
            parameters[3].Value = model.Description;
            parameters[4].Value = model.BlogUrl;
            parameters[5].Value = model.FeedUrl;
            parameters[6].Value = model.Xfn;
            parameters[7].Value = model.SortIndex;

            object obj = Helper.ExecuteScalar(CommandType.Text,strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(JinianNet.KuaiBlog.Entity.BlogRollItem model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update be_BlogRollItems set ");
            strSql.Append("BlogId=@BlogId,");
            strSql.Append("BlogRollId=@BlogRollId,");
            strSql.Append("Title=@Title,");
            strSql.Append("Description=@Description,");
            strSql.Append("BlogUrl=@BlogUrl,");
            strSql.Append("FeedUrl=@FeedUrl,");
            strSql.Append("Xfn=@Xfn,");
            strSql.Append("SortIndex=@SortIndex");
            strSql.Append(" where BlogRollRowId=@BlogRollRowId");
            SqlParameter[] parameters = {
					new SqlParameter("@BlogId", SqlDbType.UniqueIdentifier,16),
					new SqlParameter("@BlogRollId", SqlDbType.UniqueIdentifier,16),
					new SqlParameter("@Title", SqlDbType.NVarChar,255),
					new SqlParameter("@Description", SqlDbType.NVarChar,-1),
					new SqlParameter("@BlogUrl", SqlDbType.VarChar,255),
					new SqlParameter("@FeedUrl", SqlDbType.VarChar,255),
					new SqlParameter("@Xfn", SqlDbType.VarChar,255),
					new SqlParameter("@SortIndex", SqlDbType.Int,4),
					new SqlParameter("@BlogRollRowId", SqlDbType.Int,4)};
            parameters[0].Value = model.BlogId;
            parameters[1].Value = model.BlogRollId;
            parameters[2].Value = model.Title;
            parameters[3].Value = model.Description;
            parameters[4].Value = model.BlogUrl;
            parameters[5].Value = model.FeedUrl;
            parameters[6].Value = model.Xfn;
            parameters[7].Value = model.SortIndex;
            parameters[8].Value = model.BlogRollRowId;

            int rows = Helper.ExecuteNonQuery(CommandType.Text,strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int BlogRollRowId)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from be_BlogRollItems ");
            strSql.Append(" where BlogRollRowId=@BlogRollRowId");
            SqlParameter[] parameters = {
					new SqlParameter("@BlogRollRowId", SqlDbType.Int,4)
			};
            parameters[0].Value = BlogRollRowId;

            int rows = Helper.ExecuteNonQuery(CommandType.Text,strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string BlogRollRowIdlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from be_BlogRollItems ");
            strSql.Append(" where BlogRollRowId in (" + BlogRollRowIdlist + ")  ");
            int rows = Helper.ExecuteNonQuery(CommandType.Text,strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public JinianNet.KuaiBlog.Entity.BlogRollItem GetItem(int BlogRollRowId)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 BlogRollRowId,BlogId,BlogRollId,Title,Description,BlogUrl,FeedUrl,Xfn,SortIndex from be_BlogRollItems ");
            strSql.Append(" where BlogRollRowId=@BlogRollRowId");
            SqlParameter[] parameters = {
					new SqlParameter("@BlogRollRowId", SqlDbType.Int,4)
			};
            parameters[0].Value = BlogRollRowId;

            JinianNet.KuaiBlog.Entity.BlogRollItem model = new JinianNet.KuaiBlog.Entity.BlogRollItem();
            DataTable dt= Helper.ExecuteTable(CommandType.Text,strSql.ToString(), parameters);
            if (dt.Rows.Count > 0)
            {
                return DataRowToModel(dt.Rows[0]);
            }
            else
            {
                return null;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public JinianNet.KuaiBlog.Entity.BlogRollItem DataRowToModel(DataRow row)
        {
            JinianNet.KuaiBlog.Entity.BlogRollItem model = new JinianNet.KuaiBlog.Entity.BlogRollItem();
            if (row != null)
            {
                if (row["BlogRollRowId"] != null && row["BlogRollRowId"].ToString() != "")
                {
                    model.BlogRollRowId = int.Parse(row["BlogRollRowId"].ToString());
                }
                if (row["BlogId"] != null && row["BlogId"].ToString() != "")
                {
                    model.BlogId = new Guid(row["BlogId"].ToString());
                }
                if (row["BlogRollId"] != null && row["BlogRollId"].ToString() != "")
                {
                    model.BlogRollId = new Guid(row["BlogRollId"].ToString());
                }
                if (row["Title"] != null)
                {
                    model.Title = row["Title"].ToString();
                }
                if (row["Description"] != null)
                {
                    model.Description = row["Description"].ToString();
                }
                if (row["BlogUrl"] != null)
                {
                    model.BlogUrl = row["BlogUrl"].ToString();
                }
                if (row["FeedUrl"] != null)
                {
                    model.FeedUrl = row["FeedUrl"].ToString();
                }
                if (row["Xfn"] != null)
                {
                    model.Xfn = row["Xfn"].ToString();
                }
                if (row["SortIndex"] != null && row["SortIndex"].ToString() != "")
                {
                    model.SortIndex = int.Parse(row["SortIndex"].ToString());
                }
            }
            return model;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataTable GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select BlogRollRowId,BlogId,BlogRollId,Title,Description,BlogUrl,FeedUrl,Xfn,SortIndex ");
            strSql.Append(" FROM be_BlogRollItems ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return Helper.ExecuteTable(CommandType.Text,strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataTable GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" BlogRollRowId,BlogId,BlogRollId,Title,Description,BlogUrl,FeedUrl,Xfn,SortIndex ");
            strSql.Append(" FROM be_BlogRollItems ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return Helper.ExecuteTable(CommandType.Text,strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM be_BlogRollItems ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = Helper.ExecuteScalar(CommandType.Text,strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataTable GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.BlogRollRowId desc");
            }
            strSql.Append(")AS Row, T.*  from be_BlogRollItems T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return Helper.ExecuteTable(CommandType.Text,strSql.ToString());
        }


        
        

        
    }
}

