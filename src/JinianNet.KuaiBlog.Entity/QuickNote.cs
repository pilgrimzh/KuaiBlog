﻿
using System;
namespace JinianNet.KuaiBlog.Entity
{
	/// <summary>
	/// QuickNotes:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class QuickNote
	{
		public QuickNote()
		{}
		#region Model
		private int _quicknoteid;
		private Guid _noteid;
		private Guid _blogid;
		private string _username;
		private string _note;
		private DateTime? _updated;
		/// <summary>
		/// 
		/// </summary>
		public int QuickNoteId
		{
			set{ _quicknoteid=value;}
			get{return _quicknoteid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public Guid NoteId
		{
			set{ _noteid=value;}
			get{return _noteid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public Guid BlogId
		{
			set{ _blogid=value;}
			get{return _blogid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string UserName
		{
			set{ _username=value;}
			get{return _username;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Note
		{
			set{ _note=value;}
			get{return _note;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? Updated
		{
			set{ _updated=value;}
			get{return _updated;}
		}
		#endregion Model

	}
}

