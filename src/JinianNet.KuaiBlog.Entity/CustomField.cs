﻿
using System;
namespace JinianNet.KuaiBlog.Entity
{
	/// <summary>
	/// CustomFields:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class CustomField
	{
		public CustomField()
		{}
		#region Model
		private string _customtype;
		private string _objectid;
		private Guid _blogid;
		private string _key;
		private string _value;
		private string _attribute;
		/// <summary>
		/// 
		/// </summary>
		public string CustomType
		{
			set{ _customtype=value;}
			get{return _customtype;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string ObjectId
		{
			set{ _objectid=value;}
			get{return _objectid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public Guid BlogId
		{
			set{ _blogid=value;}
			get{return _blogid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Key
		{
			set{ _key=value;}
			get{return _key;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Value
		{
			set{ _value=value;}
			get{return _value;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Attribute
		{
			set{ _attribute=value;}
			get{return _attribute;}
		}
		#endregion Model

	}
}

