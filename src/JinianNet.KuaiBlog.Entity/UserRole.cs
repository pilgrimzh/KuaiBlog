﻿
using System;
namespace JinianNet.KuaiBlog.Entity
{
	/// <summary>
	/// UserRoles:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class UserRole
	{
		public UserRole()
		{}
		#region Model
		private int _userroleid;
		private Guid _blogid;
		private string _username;
		private string _role;
		/// <summary>
		/// 
		/// </summary>
		public int UserRoleId
		{
			set{ _userroleid=value;}
			get{return _userroleid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public Guid BlogId
		{
			set{ _blogid=value;}
			get{return _blogid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string UserName
		{
			set{ _username=value;}
			get{return _username;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Role
		{
			set{ _role=value;}
			get{return _role;}
		}
		#endregion Model

	}
}

