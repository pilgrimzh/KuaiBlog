﻿
using System;
namespace JinianNet.KuaiBlog.Entity
{
	/// <summary>
	/// Categories:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class Category
	{
		public Category()
		{}
		#region Model
		private int _categoryrowid;
		private Guid _blogid;
		private Guid _categoryid;
		private string _categoryname;
		private string _description;
		private Guid _parentid;
        private string _slug;
		/// <summary>
		/// 
		/// </summary>
		public int CategoryRowId
		{
			set{ _categoryrowid=value;}
			get{return _categoryrowid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public Guid BlogId
		{
			set{ _blogid=value;}
			get{return _blogid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public Guid CategoryId
		{
			set{ _categoryid=value;}
			get{return _categoryid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string CategoryName
		{
			set{ _categoryname=value;}
			get{return _categoryname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Description
		{
			set{ _description=value;}
			get{return _description;}
		}
		/// <summary>
		/// 
		/// </summary>
		public Guid ParentId
		{
			set{ _parentid=value;}
			get{return _parentid;}
		}

        /// <summary>
        /// 
        /// </summary>
        public string Slug
        {
            set { _slug = value; }
            get { return _slug; }
        }
		#endregion Model

	}
}

