﻿
using System;
namespace JinianNet.KuaiBlog.Entity
{
	/// <summary>
	/// PostTag:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class PostTag
	{
		public PostTag()
		{}
		#region Model
		private int _posttagid;
		private Guid _blogid;
		private Guid _postid;
		private string _tag;
		/// <summary>
		/// 
		/// </summary>
		public int PostTagId
		{
			set{ _posttagid=value;}
			get{return _posttagid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public Guid BlogId
		{
			set{ _blogid=value;}
			get{return _blogid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public Guid PostId
		{
			set{ _postid=value;}
			get{return _postid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Tag
		{
			set{ _tag=value;}
			get{return _tag;}
		}
		#endregion Model

	}
}

