﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace JinianNet.KuaiBlog
{
    public class BlogHttpModule : System.Web.IHttpModule
    {
        public void Dispose()
        {

        }

        public void Init(System.Web.HttpApplication context)
        {
            context.BeginRequest += BeginRequest;
        }

        private void BeginRequest(object sender, EventArgs e)
        {
            HttpApplication application = (HttpApplication)sender;
            HttpContext ctx = application.Context;
            TemplateHandler handler = TemplateHandler.CreateHandler(ctx);
            if(handler!=null)
            {
                ctx.RemapHandler(handler);
            }
        }
    }
}
