﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JinianNet.KuaiBlog
{
    public class Blog
    {
        public static Entity.Blog CurrentBlog(string host)
        {
            if (host != null)
            {
                if (blogs == null)
                {
                    lock (lockHelper)
                    {
                        blogs = new Dictionary<string, Entity.Blog>();
                        foreach (Entity.Blog node in new SQLServerDAL.Blog().GetList())
                        {
                            if (!node.IsActive)
                                continue;
                            if (!string.IsNullOrEmpty(node.Hostname))
                            {
                                blogs[node.Hostname] = node;
                            }
                            else {
                                defaultBlog = node;
                            }

                        }
                    }
                }
                Entity.Blog model;

                if (blogs.TryGetValue(host.ToLower(), out model))
                    return model;
            }
            return defaultBlog;
        }

        private static Entity.Blog defaultBlog;

        private static object lockHelper = new object();
        private static IDictionary<string, Entity.Blog> blogs;
    }
}
